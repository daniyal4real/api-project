package apiserver

type Config struct {
	BinAddr  string `toml:"bind_addr"`
	LogLevel string `toml:"log_level"`
}

func NewConfig() *Config {
	return &Config{
		BinAddr:  ":9090",
		LogLevel: "debug",
	}
}
